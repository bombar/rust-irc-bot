extern crate irc;
extern crate tokio;
extern crate futures;

use irc::client::prelude::*;
use futures::prelude::*;

// Allows main to be async
#[tokio::main]
async fn main() -> irc::error::Result<()> {

    let mut client = Client::new("./config.toml").await?;
    client.identify()?;

    let mut stream = client.stream()?;
    let sender = client.sender();


    // For use transpose see https://doc.rust-lang.org/std/option/enum.Option.html#method.transpose
    while let Some(message) = stream.next().await.transpose()? {
        // Logs to stdout
       print!("{}", message); // Don't add newline.

        match message.command {
            Command::PRIVMSG(ref target, ref msg) => {

                let prefix = [client.current_nickname(), ": "].concat();

                if msg.starts_with(&prefix) {

                    let msg = msg.strip_prefix(&prefix).unwrap();

                    if msg.starts_with("Join ") {

                        let msg = msg.strip_prefix("Join ").unwrap();
                        let chans_to_join: Vec<&str> = msg.split(" ").collect();
                        for chan in chans_to_join {
                            sender.send_join(chan)?;
                        }
                    }

                    else if msg.contains("channels") {

                        let channels = client.list_channels();
                        if let Some(chans) = &channels {
                            println!("{:?}", chans);
                            for chan in chans {
                                sender.send_privmsg(target, chan)?;
                            }
                        }
                    }

                    else {
                        sender.send_privmsg(target, "Hi!")?;
                    }
                }
            }
            _ => (),
        }
    }

    Ok(())
}
